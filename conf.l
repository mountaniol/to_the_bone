%{
#include <math.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "parsers.h"
%}

 /* This parser reads config file and generates tests for functions
  * described in ths file
  * Lexic of the config:
  * <func>
  * <name> func_name
  * <return> int
  * <arg> type=int name=a1
  * <arg> type=float name=f2
  * <static> yes
  * <test> yes
  * <follow> prev_func
  * </func>
  *
  * A single function must be described between <func> and </func> tags.
  * 
  * Let's get function: int func_name (int a1, float f2)
  * 
  * <name> is the name of the function.
  * 
  * <return> is "int"
  * 
  * Arguments of the function in <arg> tag  should appear in the same order as it goes in function.
  * 
  * <static> should be defined to "yes" if the function is statis, i.e.:
  * static int func_name (int a1, float f2)
  * 
  * <test> is "yes" if the function should be tested.
  * If <test> defined as "no" than no test will be generated for this function.
  * 
  * <follow> means that this function must be run just and only after running prev_func;
  * It is useful in case where function A locks a mutex, and function B unlocks one.
  * 
  */

 static int in_function = 0;
 int lines = 1, num_chars = 0, num_brace_open = 0, num_brace_close = 0;
 #define CHOP(c) while (*c == ' ' || *c == '\t')c++
 //#define no_printf(a, ...) do{}while(0)
 #define no_printf printf
 #define func_state 22
 #define FUNC_LEN 1024
 char func[FUNC_LEN];

 static unsigned long br_count;

 struct ll {
	char *string;
	struct ll *next;
 };
 static struct ll *_root;
 void add_string(struct ll * root, char *s);
 void clean_list(struct ll * root);
 void print_list(struct ll *root);

 void usage(char *);
 char * construct_name(char * s);

%option yylineno
 /* declaration keywords */

SPACE		[ \t]
DIGIT		[0-9]
WORD		[a-zA-Z]
ID		[a-zA-Z_][a-zA-Z0-9_]*
MACRO		#{ID}
INCLUDE 	(#include)+{SPACE}*["<"|"\""]+{SPACE}*.*+{SPACE}*[">"|"\""]+.*
DEFINE  	#{ID}*.*

SHORT_CMNT	[/]{2}.*
LONG_CMNT	([/][*])+.*([*][/])$
LONG_CMNT_B	([/][*])+
LONG_CMNT_E	.*([*][/])

 /* Start of multi-line comment */
%s ST_FUNC
%s ST_NAME ST_ARG ST_RETURN ST_STATIC ST_FOLLOW ST_TEST



 /* WE don't want case sensitive parser */
%option caseless
%%

<INITIAL>.			{}
<INITIAL>"<FUNC>"		{BEGIN(ST_FUNC); in_function = 1;}
<ST_FUNC>"</FUNC>"		{BEGIN(INITIAL); in_function = 0;}

<ST_FUNC>"<arg>"		{printf("Argument start: %s\n", yytext); BEGIN(ST_ARG);}
<ST_FUNC>"<name>"		{printf("Name start: %s\n", yytext); BEGIN(ST_NAME);}
<ST_FUNC>"<return>"		{printf("Return start: %s\n", yytext);BEGIN(ST_RETURN);}
<ST_FUNC>"<static>"		{printf("Static start: %s\n", yytext);BEGIN(ST_STATIC);}
<ST_FUNC>"<follow>"		{printf("Follow start: %s\n", yytext);BEGIN(ST_FOLLOW);}
<ST_FUNC>"<test>"		{printf("Test start: %s\n", yytext);BEGIN(ST_TEST);}

<ST_ARG>{ID}			{printf("arg: %s\n", yytext);}
<ST_NAME>{ID}			{printf("name: %s\n", yytext);}
<ST_RETURN>{ID}			{printf("return: %s\n", yytext);}
<ST_STATIC>{ID}			{printf("static: %s\n", yytext);}
<ST_FOLLOW>{ID}			{printf("follow: %s\n", yytext);}
<ST_TEST>{ID}			{printf("test: %s\n", yytext);}

\n				{if (in_function) BEGIN(ST_FUNC); else BEGIN(INITIAL);}	
.		//printf( "unrecognized character: %s\n", yytext );
%%
main(int argn, char ** args)
{
	yylex();
}


void add_string(struct ll * root, char *s)
{
	struct ll * node;
	struct ll * p;

	printf("adding word %s\n", s);

	node = malloc(sizeof(struct ll));
	node->string = strdup(s);
	node->next = NULL;
	if(!root) {
		root = node;
		return;
	}

	p = root;

	while(p->next)
		p = p->next;

	p->next = node;
}

void clean_list(struct ll * root) 
{
	struct ll * p = root;
	struct ll * _p = root;
	
	while(p) {
		if (p->string) free(p->string);
		_p = p->next;
		free (p);
		p = _p;
	}	
}

void print_list(struct ll *root) 
{
	while (root) {
		printf("%s ", root->string);
		root = root->next;
	}
	printf("\n");
}


