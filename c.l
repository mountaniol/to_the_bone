%{
#include <math.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "parsers.h"
%}

 int lines = 1, num_chars = 0, num_brace_open = 0, num_brace_close = 0;
 #define CHOP(c) while (*c == ' ' || *c == '\t')c++
 //#define no_printf(a, ...) do{}while(0)
 #define no_printf printf
 #define func_state 22
 #define FUNC_LEN 1024
 char func[FUNC_LEN];

 struct ll {
	char *string;
	struct ll *next;
 };
 static struct ll *_root;
 void add_string(struct ll * root, char *s);
 void clean_list(struct ll * root);
 void print_list(struct ll *root);

 /*
  * Ok, how we define a function definition:
  * It is an opening ID (return type) which can be missed;
  * There also can be several IDs, for example "static int" or "static struct villy_wonks * func()"
  * There can be pointer sighn "*" between function type and function name.
  * So the sequence for the beginning is:
  * 1. {ID} -> STATE_ID
  * 2. <STATE_ID> {ID} (keeps the state)
  * 3. <STATE_ID> {BRACKET_OPEN} -> STATE_BRACKET_OPEN (we found "(",  probably it is begginning of a function params)
  * 4. <STATE_BRACKET_OPEN> {ID} - we found an type of argeument, next {ID} shoould be a name
  * 5. <STATE_BRACKET_OPEN> "," - splitter of arguments
  * 6. <STATE_BRACKET_OPEN> ")" -> STATE_BRACE_CLOSE - end of func arguments
  * 7. <STATE_BRACE_CLOSE> ";" - it is not a func definition, it's something else
  * 8. <STATE_BRACE_CLOSE> "{} - Yes, it is function definition.
  */

 void usage(char *);
 char * construct_name(char * s);

%option yylineno
 /* declaration keywords */

ADDRESS		"&"
POINTER		"*"
AP		["*"|"&"]
SPACE		[ \t]
SPACE_AND_NL	[ \t\n]
FUNC_DEC_SPACES [{SPACE_AND_NL}{AP}]
DIGIT		[0-9]
ALPHA		[a-zA-Z]
ID		[a-zA-Z_][a-zA-Z0-9_]*
MACRO		#{ID}
INCLUDE 	(#include)+{SPACE}*["<"|"\""]+{SPACE}*.*+{SPACE}*[">"|"\""]+.*
DEFINE  	#{ID}*.*

SHORT_CMNT	[/]{2}.*
LONG_CMNT	([/][*])+.*([*][/])$
LONG_CMNT_B	([/][*])+
LONG_CMNT_E	.*([*][/])

 /* Start of multi-line comment */
%s ST_COMMENT_BEGIN
 /* END of multi-line comment */
%s ST_COMMENT_END

 /* When we found an ID */
%s ST_ID
 /* When we found ( */
%s ST_BRACKET_OPEN
 /* ) */
%s ST_BRACKET_CLOSE
 /* , - usualy between params in func acll of def */
%s ST_SEMICOLON
 /* { */
%s ST_BRACE_OPEN 
 /* } */
%s ST_BRACE_CLOSE

 /*
  * This state used to track when parser meets if() {} block,
  * or another one, like do {}, while {}, etc
  * We do:
  * Count all "{" and "}" until we close the last one
  * We skip all the content of such a block for now.
  */
%s ST_INSIDE_OF_BLOCK

 /*
  * We use this state to skip keywords like sizeof(...);
  * When we meet such a keyword, we skip all following up to:
  * 1. We count amount of "(" and ")" until we close the last one 
  * and
  * 2. We found the ";"
  */
%s ST_KEYWORD

%%
{SHORT_CMNT}
{LONG_CMNT_B}			BEGIN(ST_COMMENT_BEGIN);
<ST_COMMENT_BEGIN>.		{}
<ST_COMMENT_BEGIN>{LONG_CMNT_E} BEGIN(INITIAL);
{INCLUDE}			no_printf ("include:%i:%s\n",lines, yytext);
{DEFINE}			no_printf ("define:%i:%s\n", lines, yytext);
{MACRO}				no_printf ("macro:%d:%s\n",  lines, yytext);

<INITIAL>"if"			{BEGIN(ST_INSIDE_OF_BLOCK);}
<INITIAL>"else"			{BEGIN(ST_INSIDE_OF_BLOCK);}
<INITIAL>"while"		{BEGIN(ST_INSIDE_OF_BLOCK);}
<INITIAL>"do"			{BEGIN(ST_INSIDE_OF_BLOCK);}
<INITIAL>"for"			{BEGIN(ST_INSIDE_OF_BLOCK);}
<INITIAL>"sizeof"		{BEGIN(ST_KEYWORD);}
<INITIAL>"union"		{BEGIN(ST_KEYWORD);}
<INITIAL>"struct"		{BEGIN(ST_KEYWORD);}
<INITIAL>""			{}
<INITIAL>""			{}
<INITIAL>"if"			{}
<INITIAL>"if"			{}
<INITIAL>"if"			{}
<INITIAL>"if"			{}
<INITIAL>"if"			{}
<INITIAL>"if"			{}
<INITIAL>"if"			{}

 /* This is main loop which leads to function definition */

<INITIAL>{ID}			{ strncpy(func, yytext, FUNC_LEN); no_printf("INITIAL->ST_ID: %s\n", yytext); BEGIN(ST_ID); }
<ST_ID>{ID}			{ strncpy(func, yytext, FUNC_LEN); no_printf("ST_ID: %s\n", yytext);  } // Keep to be in STATE_ID
<ST_ID>";"			{ no_printf("ST_ID -> INITIAL: %s\n", yytext);  BEGIN(INITIAL);  } // Keep to be in STATE_ID
<ST_ID>{SPACE}			// Do nothing
<ST_ID>{ADDRESS}		{no_printf("INITIAL: %s\n", yytext); ;} // Do nothing
<ST_ID>{POINTER}		{no_printf("ST_ID: %s\n", yytext);}// Do nothing
<ST_ID>"("			{no_printf("ST_ID->ST_BRACKET_OPEN: %s\n", yytext); BEGIN(ST_BRACKET_OPEN);}
<ST_BRACKET_OPEN>{ID}		{no_printf("ST_BRACKET_OPEN: %s\n", yytext);}// Do nothing
<ST_BRACKET_OPEN>{DIGIT}	{no_printf("ST_BRACKET_OPEN: %s - Digit fount, reset to INITIAL\n", yytext); BEGIN(INITIAL);} // Digit inside of brackets can't be when func declared
<ST_BRACKET_OPEN>{ADDRESS}	{no_printf("ST_BRACKET_OPEN: %s\n", yytext);} // Do nothing
<ST_BRACKET_OPEN>{POINTER}	{no_printf("ST_BRACKET_OPEN: %s\n", yytext);}// Do nothing
<ST_BRACKET_OPEN>")"		{no_printf("ST_BRACKET_OPEN->ST_BRACE_CLOSE: %s\n", yytext); BEGIN(ST_BRACE_CLOSE);}
<ST_BRACE_CLOSE>"{"		{no_printf("ST_BRACKET_CLOSE->INITIAL: %s\n", yytext); printf("Function found: %s\n", func); BEGIN(INITIAL);}
<ST_BRACKET_OPEN>";"		{no_printf("ST_BRACKET_CLOSE->INITIAL: %s\n", yytext); printf("Terminated on:  %s\n", func); BEGIN(INITIAL);}


\n	
.		//printf( "unrecognized character: %s\n", yytext );
%%
main(int argn, char ** args)
{
	FILE * in_file;
	struct stat st;
	YY_BUFFER_STATE buf_state;
	int rc;
	if (argn < 2)
		usage(args[0]);

	printf("file:%s\n",args[1]);

	rc = stat (args[1], &st);
	if (rc != 0) {
		perror("stat error: ");
		exit(-1);
	}

	in_file = fopen(args[1], "r");
	if (in_file == NULL) {
		perror("can't open file: ");
		exit(-1);
	}
	buf_state = yy_create_buffer( in_file, st.st_size);
	yy_switch_to_buffer (buf_state);

	yylex();
	/* just test
	   end test */
	//printf( "N of lines = %d\n\
	N of chars = %d\n\
		N of ( = %d\n\n
				//N of ) = %d\n",lines,
				//num_chars,num_brace_open,num_brace_close);
		construct_name(args[1]);
}

void usage(char *s) {
	printf("\nerror\n");
	printf("Usage: %s file_name\n", s);

	exit(1);
}

char * construct_name(char * s) {
	char * ss;
	if (s == NULL) {
		return NULL;
	}
	ss = malloc (strlen(s) + 6);

	if (ss == NULL) {
		return NULL;
	}

	strcpy(ss,s);
	strcat(ss,".pars");
	printf("name: %s\n",ss);
	return ss;
}

#if(0)
int write_to_file(int fd, int type, int line, char *s) {
	int rc;
	if (fd < 0 || type < 0 || line < 0 || s == NULL) {
		return -EINVAL;
	}
	//write ()
}
#endif

void add_string(struct ll * root, char *s)
{
	struct ll * node;
	struct ll * p;

	printf("adding word %s\n", s);

	node = malloc(sizeof(struct ll));
	node->string = strdup(s);
	node->next = NULL;
	if(!root) {
		root = node;
		return;
	}

	p = root;

	while(p->next)
		p = p->next;

	p->next = node;
}

void clean_list(struct ll * root) 
{
	struct ll * p = root;
	struct ll * _p = root;
	
	while(p) {
		if (p->string) free(p->string);
		_p = p->next;
		free (p);
		p = _p;
	}	
}

void print_list(struct ll *root) 
{
	while (root) {
		printf("%s ", root->string);
		root = root->next;
	}
	printf("\n");
}


