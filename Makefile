
C_LEXERS  := $(shell ls *.l | sed 's/.l/.c/g')
C_TARGETS := $(shell ls *.l | sed 's/.l/.o/g')
EXEC_TARGET=bone.out

all: $(C_TARGETS)
#	gcc $(C_TARGETS) -o $(EXEC_TARGET)
  
rm:
	rm -f $(C_LEXERS) $(C_TARGETS)

%.c: %.l
	@echo Building lexers...
	flex -o$@ $^
	
%.o: %.c
	cc  $^ -o$@ -lfl
