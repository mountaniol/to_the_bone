#include <string.h>
//#include <stab.h>
#include <malloc.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

#define MAX_S	255
#define BEGIN_S	0
unsigned int count_S = 0;
char * delim_S = NULL;
char * old_delim_S = NULL;
char *S[MAX_S] = {0};

int init_S() {
	delim_S = malloc( sizeof(char) * 3);
	memset (delim_S, 0, 3);
	strcpy(delim_S," \t");
	count_S = 0;
	return 0;
}

int newdelim_S(char *s) {
	if (s == NULL)
		return -(EINVAL);

	if (old_delim_S != NULL) {
		free(old_delim_S);
	}
	old_delim_S = delim_S;
	delim_S = strdup (s);
	return 0;
}

int olddelim_S() {
	if (delim_S != NULL && old_delim_S != NULL) {
		free (delim_S);
		delim_S = old_delim_S;
		old_delim_S = NULL;
		return 0;
	}
	return - EINVAL;
}

int clean_S() {
	int i = BEGIN_S;
	/* 1. Clean ARG if there somthing */
	for (;i < count_S; i++) {
		if (S[i] == NULL) {
			goto end;
		}
		//	printf ("clean %d\n",i);
		free (S[i]);
		//	printf ("done %d\n",i);
		S[i] = NULL;
		//	printf ("nulled %d\n",i);
	}
end:
	count_S = 0;
	return 0;
}

int destroy_S() {
	//  clean_S();
	//  free(S);
	//  free(S);
	return 0;
}

int sep_str (char * s) {
	char *s_dup, *s_ret;
	//  char *delim = " \t";
	int len;
	if (s == NULL) {
		return -EINVAL;
	}
	s_dup = strdup(s);
	if (s_dup == NULL)
		return -ENOMEM;

	/* 1. Clean ARG if there something */
	if (count_S > 0)
		clean_S();

	do {
		s_ret = strsep (&s_dup, (const char *) delim_S);
		if (s_ret != NULL) {
			len = strlen(s_ret)+1;
			S[count_S] = malloc(len);
			memset(S[count_S], 0, len);
			memcpy (S[count_S++],s_ret,len - 1);
		}
	} while (s_ret != NULL);

	return 0;
}

int change_S(char * from, char * to) {
	int i;
	int len = 0;
	char *tmp;
	if (from == NULL)
		return -EINVAL;
	if (to != NULL)
		len = strlen(to);
	tmp = strdup (to);

	for (i = 0; i < count_S; i++) {
		if (strcmp(S[i], from) == 0) {
			realloc(S[i], len + 1);
			memset(S[i], 0, len+1);
			memcpy (S[i], tmp, len);
		}
	}
	return 0;
}

int print_S(void) {
	int i = BEGIN_S;
	for (; i < count_S; i++) {
		printf("%s ",S[i]);
	}
	printf("\n");

	return 0;	
}

int main (void)
{
	char * c = "mama mila ramu";
	init_S();
	sep_str(c);
	//  printf ("%s  %s  %s count = %d\n",S[0],S[1],S[2], count_S);
	print_S();
	c = "mama da nu da net da, netta";
	clean_S();
	sep_str(c);
	//  printf ("%s %s %s %s %s %s %s count = %d\n",S[0],S[1],S[2],S[3],S[4],S[5],S[6], count_S);
	print_S();
	newdelim_S(" \t,");
	perror("");
	//  printf("delim_S: %s\n", delim_S);
	sep_str(c);
	//  printf ("%s %s %s %s %s %s %s count = %d\n",S[0],S[1],S[2],S[3],S[4],S[5],S[6], count_S);
	print_S();
	olddelim_S();
	sep_str(c);
	//  printf ("%s %s %s %s %s %s %s count = %d\n",S[0],S[1],S[2],S[3],S[4],S[5],S[6], count_S);
	print_S();
	//  sep_str(c);
	//  clean_S();
	//  destroy_S();
	return 0;
}
