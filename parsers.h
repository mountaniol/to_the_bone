#define VAR_DEF		1
#define	VAR		(1<<1)
#define	FUNC		(1<<2)
#define	FUNC_DEF	(1<<3)
#define DEFINE		(1<<4)
#define MEMBER		(1<<5)
#define PMEMBER		(1<<5)
#define LABLE		(1<<6)
#define INCLUDE		(1<<7)
#define RETURN		(1<<8)
#define FILENAME	(1<<9)

#define COMMENT		(1<<12)



#define SET_FLAG(flags,f)       {flags |= f;}
#define UNSET_FLUG(flags,f)     {flags &= ~f;}
#define TEST_FLAG(flags,f)         ( (flags & f) == f )
